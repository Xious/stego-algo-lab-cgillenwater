package cgillinwater_Algo_Steganog;

import java.awt.Color;
import java.io.IOException;

import edu.neumont.ui.Picture;

public class Steganog 
{
	final int THRESH_HOLD = 32;


	public Picture embedToImage(Picture normImage, String message)
	{
		int i = 0;
		int mask = 0x03;
		int xVal = 0;
		int yVal = 0;
		

		PrimeIterator primeIt = new PrimeIterator(normImage.height() * normImage.width());

		while(i < message.length())
		{
			int prime = primeIt.next();
			if(xVal >= normImage.width())
			{
				xVal = 0;
				yVal++;
			}
			
			
			yVal = prime / normImage.width();
			xVal = prime - normImage.width() * yVal;
			Color myColor = normImage.get(xVal, yVal);

			int ascii = message.charAt(i) - THRESH_HOLD;
			int subtractMe;
			
			//changes for Red
			subtractMe = myColor.getRed() & mask;
			int newRed = myColor.getRed() - subtractMe;
			int myRed = ascii/16;
			ascii -= 16 * myRed;

			//Changes for Green
			subtractMe = myColor.getGreen() & mask;
			int newGreen = myColor.getGreen() - subtractMe;
			int myGreen = ascii/4;
			ascii -= 4 * myGreen;

			//Changes for Blue
			subtractMe = myColor.getBlue() & mask;
			int newBlue = myColor.getBlue() - subtractMe;
			int myBlue = ascii;

			myColor = new Color(newRed + myRed, newGreen + myGreen, newBlue + myBlue);
			normImage.set(xVal, yVal, myColor);
			
			i++;
			xVal++;
		}

		//Returns the new picture with your embedded message.
		return normImage;
	}

	public String giveMeTheMessage(Picture secretPic) throws IOException
	{
		String stringToReturn = "";
		secretPic.setOriginUpperLeft();
		PrimeIterator primeThisOne = new PrimeIterator(secretPic.height() * secretPic.width());

		for(int col = 0; col < secretPic.height(); col++)
		{
			for(int row = 0; row < secretPic.width(); row++)
			{
				int prime = primeThisOne.next();
				col =  prime / secretPic.width();
				row =  prime % secretPic.width();


				//Sets the color's binary implementations to strings
				Color yourColor = secretPic.get(row, col);
				String red = Integer.toBinaryString(yourColor.getRed());
				String myRed = "";
				String green = Integer.toBinaryString(yourColor.getGreen());
				String myGreen = "";
				String blue = Integer.toBinaryString(yourColor.getBlue());
				String myBlue = "";

				//Takes the last two bits of Red to read and convert to ascii later
				if(red.length() <= 1){ myRed = red; }
				else{ myRed = red.substring(red.length() - 2, red.length()); };

				//Takes the last two bits of Green to read and convert to ascii later
				if(green.length() <= 1){ myGreen = green; }
				else{ myGreen = green.substring(green.length() - 2, green.length()); };

				//Takes the last two bits of Blue to read and convert to ascii later
				if(blue.length() <= 1){ myBlue = blue; }
				else{ myBlue = blue.substring(blue.length() - 2, blue.length()); };
				
				if(myRed.length() == 1){myRed = "0" + myRed;}
				if(myGreen.length() == 1){myGreen = "0" + myGreen;}
				if(myBlue.length() == 1){myBlue = "0" + myBlue;}
				
				
				String myString = myRed + myGreen + myBlue;
				int asciiVal = Integer.parseInt(myString, 2) + THRESH_HOLD;

				if((char)asciiVal == '@')
				{
					col = secretPic.height();
					row = secretPic.width();
				}
				else
				{
					stringToReturn += (char)asciiVal;
				}

			}
		}

		return stringToReturn;
	}
}
