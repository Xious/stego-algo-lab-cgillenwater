package cgillinwater_Algo_Steganog;
import java.util.Iterator;


public class PrimeIterator implements Iterator<Integer>
{

	private boolean[] areYouPrime;
	int nextNum = 0;
	public PrimeIterator(int max)
	{
		areYouPrime = new boolean[max+1];
		for(int i = 2; i <= max; i++)
		{
			areYouPrime[i] = true;
		}
		
		for(int i = 2; i * i <= max; i++)
		{
			if(areYouPrime[i])
			{
				for(int j = i; j * i <= max; j++)
				{
					areYouPrime[i * j] = false;
				}
			}
		}
	}
	
	@Override
	public boolean hasNext() 
	{
		//Iterates through the number within areYouPrime[]
		while(nextNum < areYouPrime.length && !areYouPrime[nextNum])
		{
			nextNum++;
		}
		
		if(nextNum < areYouPrime.length)
		{
			//Means that there is another number to still iterate through
			return true;
		}
		//Exits the method
		return false;
	}

	@Override
	public Integer next() 
	{
		while(nextNum < areYouPrime.length && !areYouPrime[nextNum])
		{
			if(!areYouPrime[nextNum])
			{
				nextNum++;
			}
		}
		
		return nextNum++;
	}

	@Override
	public void remove() {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException();
	}	
}
